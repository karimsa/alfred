/**
 * index.js - alfred
 * 
 * Copyright (C) 2015 Karim Alibhai.
 **/

"use strict";

var natural = require('natural')
  , util = require('util')
  , EventEmitter = require('events').EventEmitter
  , stemmify = natural.PorterStemmer.tokenizeAndStem.bind(natural.PorterStemmer)
  , BayesClassifier = natural.BayesClassifier
  , LogisticRegressionClassifier = natural.LogisticRegressionClassifier

var alfred = {
   events: new EventEmitter()
 , cmds: []
 , cmdClassifier: new LogisticRegressionClassifier()
 , debug: true

   // .addCommand([list], [callback])
   // @param list an array of string triggers
   // @param callback a function to call on event
   // @param {optional} parse whether or not to try and parse the input
 , addCommand: function (prompts, fn, parse) {
     var i = String(alfred.cmds.push({
         prompts: prompts
       , classifier: new BayesClassifier()
       , callback: fn
       , parse: !!parse
     }) - 1)

     // train classifiers with entire list
     prompts.forEach(function (prompt) {
       alfred.cmdClassifier.addDocument(prompt, i)
       alfred.cmds[i].classifier.addDocument(prompt, prompt)
     })

     // adjust
     alfred.cmdClassifier.train()
     alfred.cmds[i].classifier.train()
   }

   // .try([input])
   // try to find a command
 , try: function (input, then) {
     input = String(input).trim().toLowerCase()
     then = then || function () {}

     var classes = alfred.cmdClassifier.getClassifications(input)
     alfred.output.log('"%s" => %s', input, JSON.stringify(classes, null, 2))

     classes.push({
         label: null
       , value: 0
     })

     if (classes[0].value === 1 / classes.length) {
       alfred.output.log('i have never heard of that before')
       return then()
     } else if ((classes[0].value - classes[1].value) < 0.2) {
       alfred.output.log('my confidence level is kinda low... (%s% vs. %s%)', classes[0].value * 100, classes[1].value * 100)
       return then()
     }

     var guess = classes[0]
       , i = parseInt(guess.label, 10)
       , fn = alfred.cmds[i].callback
       , exact = alfred.cmds[i].classifier.classify(input).split('*')
       , arg = input
       , n = exact.length

     // clean up input
     if (alfred.cmds[i].parse) {
       alfred.output.log('PARSE TRY: %j', exact)

       if (exact.length === 1) {
         exact[0].split(/\W+/).map(function (word) {
           arg = arg.replace(new RegExp(word, 'g'), '')
         })
       } else {
         var before = exact[0] === ''
           , phrase = exact[before ? 1 : 0].trim()

         if (arg.indexOf(phrase) !== -1) {
           arg = before ? arg.substr(0, arg.indexOf(phrase)) : arg.substr(arg.indexOf(phrase) + phrase.length)
         }
       }

       arg = arg.replace(/\s+/, '')
     }

     // log
     alfred.output.log('i am %s% sure you want me to "%s" in "%j" [%s]', guess.value * 100, exact, alfred.cmds[i].prompts, guess.label)
     var hdl = fn.call(alfred, input)
       , thing
       , next = function () {
           if (thing.done) {
             then()
           } else {
             alfred.input.get(thing.value, function (input) {
               thing = hdl.next(input)
               next()
             })
           }
         }

     // manual generator execution
     thing = hdl.next()

     if (thing.done) {
       if (thing.value) alfred.output.say(thing.value)
       then()
     } else next()
   }

 , input: {
     get: function (input, then) {
       alfred.output.say(input)
       alfred.events.once('input', then)
       alfred.events.emit('get-input')
     }
   }

 , output: {
     log: require('util').debuglog('alfred') /*function () {
       if (alfred.debug) {
         process.stdout.write('[ALFRED] ')
         console.log.apply(console, arguments)
       }
     }*/,

     say: function () {
       var msg = util.format.apply(util, arguments)
       alfred.events.emit('say', msg)
       alfred.output.log('spoke: %s', msg)
     }
   }

 , init: function (cmds) {
     // add default commands
     cmds.forEach(function (command) {
       var callback = command.fn || function () {}
       if (typeof command.script === 'string') callback = require(command.script) || callback;

       alfred.addCommand(command.prompts, function* () {
         try {
           var hdl = callback.apply(this, arguments),
              thing = hdl.next();
              
           while (!thing.done) thing = hdl.next(yield thing.value);
         }
         catch (e) { console.error(e); console.error(e.stack); }
       }, command.parse)
     })

     return alfred
   }
}

// add shutdown command
alfred.addCommand(['shutdown now'], function () {
  process.exit(-1)
})

module.exports = alfred
