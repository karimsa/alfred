var alfred = require('../../')
var prompt = function () {
  process.stdout.write('\nalfred> ')
  process.stdin.resume()
  process.stdin.once('data', function (chunk) {
    process.stdin.pause()
    alfred.try(chunk, prompt)
  })
}

// get commands
alfred.init([
    {
        prompts: ['hello']
      , script: __dirname + '/hello.js'
    }
])

// enable custom output
alfred.debug = false
alfred.events.on('say', function (text) {
  console.log('[alfred] %s', text)
})

// enable custom input
alfred.events.on('get-input', function () {
  process.stdin.resume()
  process.stdout.write('      => ')
  process.stdin.once('data', function (chunk) {
    process.stdin.pause()
    chunk = chunk.trim()

    alfred.events.emit('input', chunk.toLowerCase())
  })
})

// assume all data to be utf8
process.stdin.setEncoding('utf8')

// begin
prompt()
