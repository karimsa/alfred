// hello.js - alfred
// say hello
//
// Copyright (C) 2015 Karim Alibhai.

module.exports = function* (input) {
  this.output.say('hello, master')
}
